ActiveAdmin.register Flight do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
  permit_params do
   permitted = [:permitted,:from,:to,:departure,:first_class_seats,:bussiness_class_seats,
    :economy_class_seats,:avaible_seats_first_class,:avaible_seats_bussiness_class,:avaible_seats_sconomy_class,
    :price_first_class,:price_bussiness_class,:price_economy_class,:approve]
   #permitted << :other if params[:action] == 'create' && current_user.admin?
   permitted
 end

end
