class FlightsController < ApplicationController
  
  layout 'home'

  before_action :authenticate_user!
  skip_before_action :verify_authenticity_token

  def index
    f = Flight.new
    @ticket = Ticket.new
    @flights = f.search(params[:from],params[:to])
    @un_approved = Flight.where(approve:false)
  end

  def show
    @flight = Flight.find(1)
  end

  def new
    @flight = Flight.new
  end

  def approve
  @flight = Flight.find(params[:id])
  @flight.update(approve: true)
  redirect_to flights_path
  end 
  def create
    @flight = Flight.new(flight_params)
    if @flight.save
      redirect_to(flight_path(@flight))
    else
      render('new')
    end
  end

  def edit
    @flight = Flight.find(params[:id])
  end

  def update
    @flight = Flight.find(params[:id])
    if @flight.update_attributes(flight_params)
      redirect_to(flight_path(@flight))
    else
      render('edit')
  end
end

  def destroy
    @flight = Flight.find(params[:id])
    @flight.destroy
  end

  def buy
    @flight = Flight.find(params[:id])  
    @user = current_user
  end

  def bought
    @flight = Flight.find(params[:id])
    @number_of_tickets = params[:tickets][:number_of_tickets]
    @seat_type = params[:tickets][:seat_type]
    @seat_type = params[:tickets][:seat_type]
    @user = current_user.id
    puts @seat_type.class
    Ticket.create(user_id: @user,flight_id: @flight.id,number_of_tickets: @number_of_tickets,seat_type: @seat_type)
     
     if @seat_type == "1"
        puts "hello 1"
        @update_seat_count = @flight.avaible_seats_first_class.to_i - @number_of_tickets.to_i;
        puts @update_seat_count
        @flight.update(avaible_seats_first_class: @update_seat_count)
     elsif @seat_type == "2"
      puts "hello 2"
        @update_seat_count = @flight.avaible_seats_bussiness_class.to_i - @number_of_tickets.to_i;
        @flight.update(avaible_seats_bussiness_class: @update_seat_count)
        puts @update_seat_count
     elsif @seat_type == "3"
      puts "hello 3"
        @update_seat_count = @flight.avaible_seats_sconomy_class - @number_of_tickets;
        @flight.update(avaible_seats_sconomy_class: @update_seat_count)
        puts @update_seat_count
     end

      redirect_to(congratulation_flight_path)
  end

  def congratulation
  end

  def flight_params
    params.require(:flight).permit(:from,:to,:departure,:first_class_seats,:bussiness_class_seats,
    :economy_class_seats,:avaible_seats_first_class,:avaible_seats_bussiness_class,:avaible_seats_sconomy_class,
    :price_first_class,:price_bussiness_class,:price_economy_class,:approve)
  end



end