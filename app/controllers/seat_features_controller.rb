class SeatFeaturesController < ApplicationController

   layout 'home'
  def index
    @features1 = SeatFeature.list(1)
    @features2 = SeatFeature.list(2)
    @features3 = SeatFeature.list(3)
  end

  def new
    @feature = SeatFeature.new()
  end

  def create
      @feature = SeatFeature.new(seat_feature_params)
      if @feature.save
        redirect_to(seat_features_path)
      else
        render('new')
      end
  end

  def edit
    @feature = SeatFeature.find(params[:id])
  end

  def update
    @feature = SeatFeature.find(params[:id])
    if @feature.update_attributes(seat_feature_params)
      redirect_to(seat_features_path)
    else
      render('edit')
    end
  end

  def delete
    @feature = SeatFeature.find(params[:id])
  end

  def destroy
    @feature = SeatFeature.find(params[:id])
    @feature.destroy
    redirect_to(seat_feature_path)
  end

 def seat_feature_params
   params.require(:seat_feature).permit(:seat_id,:feature)
 end

end
