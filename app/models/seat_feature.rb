class SeatFeature < ApplicationRecord
  belongs_to :seat

  scope :sorted, lambda { order(:seat_id => 'ASC')}
  scope :list, lambda {|query| where(["seat_id = ?","#{query}"])}

end
