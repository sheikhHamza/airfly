class Flight < ApplicationRecord

  has_many :seats
  has_many :tickets
  has_many :users, through: :tickets

  scope :sorted, lambda {order(:departure)}

  def sorted_by_location(f,t)
    Flight.where(:from => f).where(:to => t).where(approve: true).order(:departure)
  end

  def search(from,to)
    if from and to
        sorted_by_location(from,to)
      else
        Flight.where(approve: true)
      end
  end

end
