class Ticket < ApplicationRecord
	belongs_to :user
	belongs_to :flight
	has_one :seat
end
