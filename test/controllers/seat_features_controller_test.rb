require 'test_helper'

class SeatFeaturesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get seat_features_index_url
    assert_response :success
  end

  test "should get show" do
    get seat_features_show_url
    assert_response :success
  end

  test "should get new" do
    get seat_features_new_url
    assert_response :success
  end

  test "should get edit" do
    get seat_features_edit_url
    assert_response :success
  end

  test "should get delete" do
    get seat_features_delete_url
    assert_response :success
  end

end
