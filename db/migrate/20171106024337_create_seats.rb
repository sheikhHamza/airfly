class CreateSeats < ActiveRecord::Migration[5.1]
 def up
  create_table :seats do |t|
     t.string 'type'
     t.timestamp
    end
 end

  def down
    drop_table :seats
  end

end
