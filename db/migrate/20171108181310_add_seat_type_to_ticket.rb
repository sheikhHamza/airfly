class AddSeatTypeToTicket < ActiveRecord::Migration[5.1]
  def change
    add_column :tickets, :seat_type, :integer
  end
end
