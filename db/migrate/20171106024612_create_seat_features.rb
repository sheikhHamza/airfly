class CreateSeatFeatures < ActiveRecord::Migration[5.1]
  def up
    create_table :seat_features do |t|
      t.integer 'seat_id'
      t.string 'feature'
      t.timestamps
    end
    add_index("seat_features",'seat_id')
  end
  def down
    drop_table :seat_features
  end
end
