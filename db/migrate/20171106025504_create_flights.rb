class CreateFlights < ActiveRecord::Migration[5.1]
  def up
    create_table :flights do |t|
      t.string 'from'
      t.string 'to'
      t.datetime 'departure'
      t.integer 'first_class_seats'
      t.integer 'bussiness_class_seats'
      t.integer 'economy_class_seats'
      t.integer 'avaible_seats_first_class'
      t.integer 'avaible_seats_bussiness_class'
      t.integer 'avaible_seats_sconomy_class'
      t.integer 'price_first_class'
      t.integer 'price_bussiness_class'
      t.integer 'price_economy_class'
      t.boolean 'approve', :default => false
      t.timestamps
    end
    add_index('flights','from')
    add_index('flights','to')
  end

  def down
    drop_table :flights
  end
end
